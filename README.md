SPACEINATOR
Version 1.0.0

Presses the space bar once randomly within a user-defined set of time (in minutes).
Additional seconds are tacked on to the lower-bound number to add randomness.
